const express = require('express')
const consola = require('consola')
const xlsxFile = require('read-excel-file/node');
const { Nuxt, Builder } = require('nuxt')
const app = express()

// Import and Set Nuxt.js options
const config = require('../nuxt.config.js')
config.dev = process.env.NODE_ENV !== 'production'
const getData = async () => {
  const rows = await xlsxFile(__dirname+'/source-data.xlsx', { sheet: 4 })
  console.log('Update Data... ',new Date())
  const selectRow = rows.filter( (item, index) => index >= 0 && index <= 225 && item[0] !== null )
  const data = selectRow.map( (item, idex) => {
    return item.filter( (item, index) => index <= 11 )
  }  )
  return data
}

app.get('/api/getdata', async (req, res) => {
  const data = await getData()
  return res.send(data)
})

app.use('/api', function (req, res) {
  res.send('OK')
});

async function start () {
  // Init Nuxt.js
  const nuxt = new Nuxt(config)

  const { host, port } = nuxt.options.server

  await nuxt.ready()
  // Build only in dev mode
  if (config.dev) {
    const builder = new Builder(nuxt)
    await builder.build()
  }

  // Give nuxt middleware to express
  app.use(nuxt.render)

  // Listen the server
  app.listen(port, host)
  consola.ready({
    message: `Server listening on http://${host}:${port}`,
    badge: true
  })
}
start()
